% Compare model data and data captured by wireshark
clear all;
load modelData       % 
load wiresharkdata   % matrix "pkt2"

% Get first packet from the model
model = beamformPacket(1,1,1,1,1).dataUnscaled;
% and first packet from the firmware
firmware = pkt2;


% firmware scale factor
firmwareScale = firmware(1,1);
% drop the scale factor so we have just the data
firmware = firmware(2:end,:); 

% swap even and odd in the firmware version so it matches the model
firmware_even = firmware(1:2:end,:);
firmware_odd = firmware(2:2:end,:);

firmwareSwapped = firmware;
firmwareSwapped(1:2:end) = firmware_odd;
firmwareSwapped(2:2:end) = firmware_even;

% convert to complex
firmwarePol0 = firmwareSwapped(:,2) + 1i * firmwareSwapped(:,1);
firmwarePol1 = firmwareSwapped(:,4) + 1i * firmwareSwapped(:,3);

% scale the model as per the firmware scaling
modelScaled = model * firmwareScale/2^9;  % 2^9 is an in-built scaling factor in the firmware

% Extract pol1 and pol2 from the model
for fs = 1:24
    modelPol0((fs-1)*32+1 : (fs-1)*32 + 32) = modelScaled((fs-1)*64+1 : (fs-1)*64 + 32);
    modelPol1((fs-1)*32+1 : (fs-1)*32 + 32) = modelScaled((fs-1)*64+33 : (fs-1)*64 + 64);
end

figure(1);
clf;
hold on;
grid on;
plot(real(firmwarePol0),'r.-');
plot(real(modelPol0),'go-');
title('wireshark red, model green');

figure(2);
clf;
hold on;
grid on;
plot(imag(firmwarePol0),'r.-');
plot(imag(modelPol0),'go-');
title('wireshark red, model green');



