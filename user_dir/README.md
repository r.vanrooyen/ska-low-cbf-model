# Quick reference to set up and use the Low CBF octave model

The packetised model is a more detailed description of the function of the firmware and control software.
It is suitable for use in firmware and software development.
The packetised model includes functionality for:
* Conversion of the parameters provided to local monitoring and control into the register values used in the firmware.
* Generation of the packets passed between the firmware modules, including LFAA data coming into the system. This is used to verify the firmware, both in simulation and when debugging in the real system.
* Generation of default values for ROMs used in the firmware e.g. filter coefficients.

## Installation
Summary for setting up to run the code using Octave as per [Model - How to use](https://confluence.skatelescope.org/display/SE/Model+-+How+to+use)     
* Clone jsonlab (ie `git clone https://github.com/fangq/jsonlab.git`) somewhere in your local filesystem
* Make a note of the path to the `jsonlab` repo (`cd jsonlab ; pwd`)
* Clone the git repository containing the model: `git clone https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-model.git`
* Change into the `ska-low-cbf-model/src_atomic` directory
* Create subdirectories here for model data generation


## Setting up and executing the model software
### Model inputs
There are four input files to configure the model

**stations.txt**    
Subarray layout described in term of the location of each of the LFAA stations.
The location per station is defined as East and North offset value [degrees] relative to the center point of the array defined by latitude and longitude.    
Details in [Station Locations](https://confluence.skatelescope.org/display/SE/Station+Locations)    
* Defines to location and layout of a subarray
* As well as the location of each of the FLAA stations associated to an array (a subarray)
* as offset values from center in meters in ENU coordinates
```
{
  latitude: center point of the array [degrees]
  longitude: center point of array [degrees]
  altitude: altitude for all stations are assumed to be the same [meters] 
  offsets: array of EN offsets of stations from center point
    [[dE1, dN1], row index 1: [offset East, offset North] 
     [-dE2, -dN2], row index 2: [offset West, offset South]
      ...,
     [dEn, dNn]]
}
```
*Note*: Row indexing starts at 1 (not 0), keep this in mind since the indices are used in the array configuration file

Example station.txt file shows a simple subarray consisting of 6 stations.


**sky.txt**  
Observation target list is defined as a list of beam pointing directions.
Objects in the sky are defined by right ascension and declination, and can be sinusoids or broadband noise.  
Details in [Sky Definition](https://confluence.skatelescope.org/display/SE/Sky+Definition)

```
{
 index: unique identifier used in array configuration to create station beams

 # source sky position (beam boresight pointing direction) 
 ascension: source right ascension [degrees]
 declination: source declination [degrees]

 # rate of travel of observation source
 rate: speed at which the object travels, 1 = sidereal rate

 # Coarse and fine parameters described the channel indices of the desired center frequency
 coarse: interger index of coarse channel with center frequency closest to the sky frequency of interest (see CSP-LFAA-ICD)
 fine: interger index of find channel with center frequency closest to the sky frequency of interest given coarse channel index, 0 = center of coarse channel

# RMS level for the source/signal (noise)
 rms: number ranging between 10 and 40 represent normal operations

 # describe the characteristics of the target observed
 sinusoids: number of sinusoids to use for the object
   [0] = broadband noise (diffuse emission)
   [1] = single tone at center frequency
   [>1] = CWs distributed around center frequency
 polarisation: Stokes [I, Q, U, V]
   [1,0,0,0] unpolarised
   [1,1,0,0] horizontally polarized
   [1,-1,0,0] vertically polarized
   [1,0,1,0] linearly polarized 45 degrees
 BW: FWHM of source in number of fine channels
 
 # describe beam FOV (image width) as well as add targets toFOV
 skyImage: targets to observe
   "" = empty string assumed single source in the middel of the FOV
   "PNG" = every pixel > 0 is a separate target with the brightness of the pixel the relative "power" of the source
 skyImageDimension: FOV in degrees (FWHM of beam)
}
```

* To place a target directly overhead at time t=0, set right ascension to -90 [degrees]. Note this is also when the highest rate of change for delay updates will be.
* Maximum delay correction (longest projected baseline) will occur at t=0 if right ascension is set to 0


**arrayConfig.txt**    
Subarray configuration and correlator mode parameters describe the CBF setup, while and target indexing are used to define the beam pointing directions.    
Details on [Array Configuration](https://confluence.skatelescope.org/display/SE/Array+Configuration)

* Choose observation mode by selecting the appropriate stationType, depending on channelisation and subarray needs
* Assign stations via station.txt:offset row indices (starting from 1)
* Calculate and assign logical IDs associated with station/substation division available for the selected station type.

This seems to be a required always on parameter (needs to be checked, use default '1')   
```
  global:  
    PSSMode: mode setting number of PSS beams
      1 = 500 PSS beams total BW 112.5 MHz
      2 = 250 PSS beams total BW 225 MHz
```` 

Station/substation configuration description   
Each station can be split into <=4 substations, with a
* station of type 300MHz can only have 1 substation per station,
* station of type 75MHz can have at most 2 substations per station and
* station of type 18.75MHz can have at most 4 substations per station    
```
  stations: [
    { 
      # associated with course channelisation
      stationType:
        ["300MHz",   # 384 coarse channels and one substation per station
         "75MHz",    # 96 coarse channels and 2 substations per station
         "18.75MHz"] # 24 coarse channels and 4 substations per station
      location: # row index of station offset parameters in station.txt
      logicalIDs: # start index of logical IDs per substations assigned to the station
      polarisationOffsets: # delay offset of V pol relative to H pol to emulate different cable lenghtss
    }
  ]
```

Subarray configuration description
```
  subArray: [
    {
      index: unique identifier for this subarray (1 to 16)
      stationType: same as the stationType of the associated stations allocated to this array (300MHz, 75MHz, 18.75MHz))
      logicalIDs: Logical IDs of the substations
        all = use all stations in this subarray
        [...] = selected logicalIDs as listed in the station.txt config.
     }
  ]
```

Configurations of station beams
```
  stationBeam: [
    {
      index: unique identifier for this beam (1 to 8)
      subArray: the index of the subarray this beam is attached to (subArray.index)
      skyIndex: the index of the sky object this beam is attached to (Sky.index in sky.txt config file)
      channels: 0 = use full bandwidth contiguously for 300 MHz stationType
    }
  ]
```

Setting up a simple correlator generating visibilites
```
  correlator:[
    {
      subArray: index of the subarray the correlator is associated with (stationBeam.subArray)
      stationBeam: index of the station beam the correlator is receiving data from (stationBeam.index)
      mode: SPECTRAL_LINE to generate visibilities over the full bandwidth
    }
  ]
```


**modelConfig.txt**    
The model setup is defined in modelConfig.txt, in the run directory. It holds parameters related to the running of the simulation model.

Required parameters    
```
{
  snr: SNR of the data in dB (>100 indicate zero noise)
  time: start time of the observation (in hours, float)
  runtime: number of integration periods (1 integration period = 0.9s)
  dataTime: simulation parameter to quickly generate large data files
  configuration: parameter lists the LFAA frequency channels to be processed
  runCorrelator: 1=run correlator, 0=skip it
  runPSS: 1=run PSS processing, 0=skip it
  runPST: 1=run PST processing, 0=skip it
}
```


### Run model matlab files
To generate configuration and JSON files from model input
```
octave
addpath('/home/ruby/jsonlab')

create_config('run2', 1, 1)
create_config('run1', 1, 1)
```

-fin-
