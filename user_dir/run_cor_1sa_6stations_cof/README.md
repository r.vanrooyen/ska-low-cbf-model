Basic steps to generate COF single target PCAP
```
cd /home/ruby/ska-low-cbf-model/src_atomic
octave
addpath('/home/ruby/jsonlab')
rundir="run_cor_1sa_6stations_cof"
# create simulated config and data
create_config(rundir, 0, 3)
# generate RAW files
get_LFAA_raw(rundir)
```
```
# generate PCAP file
cd /home/ruby/ska-low-cbf-model/python
ln -s ../../user_dir/run_cor_1sa_6stations_cof
python3 raw_to_pcap.py run_cor_1sa_6stations_cof/LFAAHeaders_fpga1.raw run_cor_1sa_6stations_cof/LFAAData_fpga1.raw run_cor_1sa_6stations_cof/LFAATestPCAP_cof_1ch_10s.pcap
```
```
cd /home/ruby/ska-low-cbf-model/user_dir/run_cor_1sa_6stations_cof
octave
addpath('/home/ruby/jsonlab')
# extract delays
readDelays("CORregisterSettings.txt")

mv cor_ctl_delays.txt LFAATestPCAP_cof_1ch_10s.txt
```
