Ipython notebooks as examples for using the CBF simulator matlab code to generate test input files for integration tests

Notebooks are developed using Google Colab and can be uploaded and executed in [Colab](https://chrome.google.com/webstore/detail/open-in-colab/iogfkhleblhcpcekbiedikdehleodpjo)
