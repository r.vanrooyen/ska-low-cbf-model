###############################################################################
# 
###############################################################################

"""
Author:
. Daniel van der Schuur, 26 June 2023
Purpose:
. Display information from captured LFAA packets
. This is a modified version of tc_apertif_unb1_correlator_offload.py
Description:
. Processes PCAP dump files stored by e.g. tcpdump.
Usage:
. Capture a PCAP file: 
  . sudo /usr/sbin/tcpdump -vvxSnelfi eth1 -n dst port 4000 -c 1000 -w ~/my_dump_file.pcap
. Examine a PCAP file with this script:
  . python pcap_reader_LFAA.py ~/my_dump_file.pcap
  . Use Grep to filter out printed lines/header fiels of interest e.g. '| grep timestamp'
Extra:
. Print back a dumped PCAP file to the screen (example: 1 packet, -c 1):
  . sudo /usr/sbin/tcpdump -e -c 1 -vvX -r ~/my_dump_file.pcap 
Info:
 . Used ICD: SKA1 LFAA TO CSP INTERFACE CONTROL DOCUMENT 100-000000-004
 . USed PCAP file: LFAATestData_cor_1sa_6stations_cof.pcap
"""


import struct
from common import *
import sys
import itertools

NOF_POLS = 2
NOF_COMPLEX = 2
NOF_STATIONS = 6
NOF_PACKETS_PER_STATION = 4080
NOF_PACKETS = NOF_PACKETS_PER_STATION*NOF_STATIONS #24480 packets

def dumpfile_to_array(dumpfile):
    """
    Returns a list of dimensions [NOF_PACKETS][8192 bytes]
    """
    f = open(dumpfile, "rb")

    # Our 2d list to return
    timesamples_list = []

    # Strip off the PCAP header
    pcap_global_hdr_raw = f.read(24)

    # Examine packets but don't append to return list until after channel 0 is found
    append_channels = False

    for packet in range(NOF_PACKETS):
        pcap_pkt_hdr_raw = f.read(16)
        eth_hdr_raw = f.read(14)
        ip_hdr_raw = f.read(20)
        udp_hdr_raw = f.read(8)
        spead_hdr_raw = f.read(72)
        timesamples_raw = f.read(8192)

        ###########################################################################
        # Ethernet header: 7 big endian unsigned shorts (16b)
        ###########################################################################
        eth_hdr_struct = struct.unpack('>7H', eth_hdr_raw)
        eth_hdr_shorts = CommonShorts(0, 7)
        for short_index,short in enumerate(reversed(eth_hdr_struct)):
            eth_hdr_shorts[short_index] = short
        print 'Packet', packet, '-', 'ETH Destination MAC         ', hex(eth_hdr_shorts[6:4])
        print 'Packet', packet, '-', 'ETH Source MAC              ', hex(eth_hdr_shorts[3:1])
        print 'Packet', packet, '-', 'ETH Ether type              ', hex(eth_hdr_shorts[0])
    
        ###########################################################################
        # IP header: 10 big endian shorts (16b) = 160 bits
        ###########################################################################
        ip_hdr_struct = struct.unpack('>10H', ip_hdr_raw)
        ip_hdr_shorts = CommonShorts(0, 10)
        for short_index,short in enumerate(reversed(ip_hdr_struct)):
            ip_hdr_shorts[short_index] = short
    
        # Convert this to CommonBits so we can use bit indexing
        ip_hdr_bits = CommonBits(ip_hdr_shorts.data, 160)
        print 'Packet', packet, '-', 'IP version                  ', hex(ip_hdr_bits[159:156])
        print 'Packet', packet, '-', 'IP header length            ',     ip_hdr_bits[155:152]
        print 'Packet', packet, '-', 'IP services                 ', hex(ip_hdr_bits[151:144])
        print 'Packet', packet, '-', 'IP total length             ',     ip_hdr_bits[143:128]
        print 'Packet', packet, '-', 'IP identification           ', hex(ip_hdr_bits[127:112])
        print 'Packet', packet, '-', 'IP flags                    ', hex(ip_hdr_bits[111:109])
        print 'Packet', packet, '-', 'IP fragment offset          ', hex(ip_hdr_bits[108:96])
        print 'Packet', packet, '-', 'IP time to live             ', hex(ip_hdr_bits[95:88])
        print 'Packet', packet, '-', 'IP Protocol                 ', hex(ip_hdr_bits[87:80])
        print 'Packet', packet, '-', 'IP header checksum          ',     ip_hdr_bits[79:64]
        print 'Packet', packet, '-', 'IP source address           ', hex(ip_hdr_bits[63:32])
        print 'Packet', packet, '-', 'IP destination address      ', hex(ip_hdr_bits[31:0])
    
        ###########################################################################
        # UDP header: 4 big endian shorts (16b)
        ###########################################################################
        udp_hdr_struct = struct.unpack('>4H', udp_hdr_raw)
        udp_hdr_shorts = CommonShorts(0, 4)
        for short_index,short in enumerate(reversed(udp_hdr_struct)):
            udp_hdr_shorts[short_index] = short
        print 'Packet', packet, '-', 'UDP destination port        ', udp_hdr_shorts[3]
        print 'Packet', packet, '-', 'UDP source port             ', udp_hdr_shorts[2]
        print 'Packet', packet, '-', 'UDP total length            ', udp_hdr_shorts[1]
        print 'Packet', packet, '-', 'UDP checksum                ', udp_hdr_shorts[0]
    
        ###########################################################################
        # SPEAD header: 72 big endian bytes
        ###########################################################################
        spead_hdr_struct = struct.unpack('>72B', spead_hdr_raw)
        spead_hdr_bytes = CommonBytes(0, 72)
        for byte_index,byte in enumerate(reversed(spead_hdr_struct)):
            spead_hdr_bytes[byte_index] = byte

        print 'Packet', packet, '-', 'spead_magic_number            ', hex(spead_hdr_bytes[71])    # 0x53
        print 'Packet', packet, '-', 'spead_version                 ', hex(spead_hdr_bytes[70])    # 0x4
        print 'Packet', packet, '-', 'spead_item_pointer_width      ', hex(spead_hdr_bytes[69])    # 0x2
        print 'Packet', packet, '-', 'spead_heap_address_width      ', hex(spead_hdr_bytes[68])    # 0x6
        print 'Packet', packet, '-', 'spead_reserved                ', hex(spead_hdr_bytes[67:66]) # 0x0
        print 'Packet', packet, '-', 'spead_number_of_items         ', hex(spead_hdr_bytes[65:64]) # 0x8
        print 'Packet', packet, '-', 'spead_heap_counter            ', hex(spead_hdr_bytes[63:62]) # 0x8001. Expected 0x0001. Set MSbit?
        print 'Packet', packet, '-', 'spead_logical_channel_id      ', hex(spead_hdr_bytes[61:60]) # 0x0
        print 'Packet', packet, '-', 'spead_packet_counter          ', hex(spead_hdr_bytes[59:56]) # 1 SPEAD packet = 6 Ethernet packet. Total: 4080 SPEAD packets per station.
        print 'Packet', packet, '-', 'spead_pkt_len                 ', hex(spead_hdr_bytes[55:54]) # 0x8004 = 32772. Expected 32768, off by 4 bytes?
        print 'Packet', packet, '-', 'spead_packet_payload_length   ', hex(spead_hdr_bytes[53:48]) # 0
        print 'Packet', packet, '-', 'spead_sync_time               ', hex(spead_hdr_bytes[47:46]) # 0x9027, expected 0x1027. MSbit set?
        print 'Packet', packet, '-', 'spead_unix_epoch_time         ', hex(spead_hdr_bytes[45:40]) # all packets are 0x6489c7b2 .. 0x6489c7e6 (6 epoch times)
        print 'Packet', packet, '-', 'spead_timestamp               ', hex(spead_hdr_bytes[39:38]) # 0x9600. Expected 0x1600. Again MSbit set?
        print 'Packet', packet, '-', 'spead_timestamp_ns            ', hex(spead_hdr_bytes[37:32]) # increments by 0x1b0000 (1.769.472 ns) per 6 packets. Expected to get duplicate timestamps for 384 channels, so apparently there is only 1 channel in the data????
        print 'Packet', packet, '-', 'spead_center_freq             ', hex(spead_hdr_bytes[31:30]) # 0x9011, expected 0x1011. Again MSbit set
        print 'Packet', packet, '-', 'spead_frequency               ', hex(spead_hdr_bytes[29:24]) # 0x12a05f20 = 312.500.000 Hz = 312.5 MHz
        print 'Packet', packet, '-', 'spead_csp_channel_info        ', hex(spead_hdr_bytes[23:22]) # ICD seems wrong here; below 3 fields are not subfields of this one.
        print 'Packet', packet, '-', 'spead_reserved                ', hex(spead_hdr_bytes[21:20]) # 0x0
        print 'Packet', packet, '-', 'spead_beam_id                 ', hex(spead_hdr_bytes[19:18]) # 0x1
        print 'Packet', packet, '-', 'spead_frequency_id            ', hex(spead_hdr_bytes[17:16]) # 0x190 = 400
        print 'Packet', packet, '-', 'spead_csp_antenna_info        ', hex(spead_hdr_bytes[15:14]) # 0xb001
        print 'Packet', packet, '-', 'spead_substation_id           ', hex(spead_hdr_bytes[13])    # 0x1
        print 'Packet', packet, '-', 'spead_subarray_id             ', hex(spead_hdr_bytes[12])    # 0x1
        print 'Packet', packet, '-', 'spead_stationID               ', hex(spead_hdr_bytes[11:10]) # 1..6 repeating
        print 'Packet', packet, '-', 'spead_nof_contributing_antenna', hex(spead_hdr_bytes[9:8])   # 0x100 = 256
        print 'Packet', packet, '-', 'spead_sample_offset           ', hex(spead_hdr_bytes[7:6])   # 0xb300. Expected 0x3300. Again MSbit set
        print 'Packet', packet, '-', 'spead_payload_offset          ', hex(spead_hdr_bytes[5:0])   # 0x0

        ###########################################################################
        # Payload: 2048 samples * little endian (ICD does not specify!) signed chars
        # [6 station packets]*[8 channel packets][2048 timesamples][Vreal, Vimag, Hreal, Himag]
        ###########################################################################
        timesamples_struct = struct.unpack('>8192b', timesamples_raw)

        # Convert to [2048 timesamples][Vreal, Vimag, Hreal, Himag]
        timesamples_list.append(timesamples_struct)

    return timesamples_list

################################################################################
# main on execution of this file
################################################################################
if __name__ == '__main__':

    # Read the dump file and put its content in a 2d array
    dump_arr = dumpfile_to_array(sys.argv[1])

    # Convert data to Numpy array [NOF_PACKETS_PER_STATION][6 STATIONS][2048 timesamples][Vreal, Vimag][Hreal, Himag]
    import numpy as np
    arr = np.array(dump_arr).reshape(NOF_PACKETS_PER_STATION, NOF_STATIONS, 2048, NOF_POLS, NOF_COMPLEX)
    print arr.shape

    # Convert real,imag dimensions into complex array
    c_arr = arr[...,0] + arr[...,1] * 1j
    print c_arr

    # Do what you like with the Numpy array
    # - Insert Numpy magic here - 

